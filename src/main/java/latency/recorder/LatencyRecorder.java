package latency.recorder;

import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.util.*;

public class LatencyRecorder {
    private static List<Double> latencies = new ArrayList<>();
    private static final Double ZERO = 0.0;
    private static final Percentile percentile = new Percentile();

    public static void reset() {
        latencies.clear();
    }

    public static List<Double> getLatencies() {
        return latencies;
    }

    public static int latencySize() {
        return latencies.size();
    }

    /**
     * Adds new latency values to latencies structure
     *
     * @param latency in milliseconds
     */

    public static void addLatency(Double latency) {
        latencies.add(latency);
    }

    public static Double getMax() {
        return (latencySize() > ZERO) ? (Collections.max(latencies)) : ZERO;
    }

    public static Double getMin() {
        return (latencySize() > ZERO) ? (Collections.min(latencies)) : ZERO;
    }

    public static double getLatencyProcent(double p) {
        Collections.sort(latencies);
        percentile.setData(
                latencies.stream()
                        .mapToDouble(Double::doubleValue)
                        .toArray()
        );
        return percentile.evaluate(p);
    }

    public static double getMean(){
        Mean mean = new Mean();
        mean.setData(latencies.stream()
                .mapToDouble(Double::doubleValue)
                .toArray());
        return mean.evaluate();
    }
}