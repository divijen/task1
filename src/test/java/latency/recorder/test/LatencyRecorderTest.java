package latency.recorder.test;

import latency.recorder.LatencyRecorder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class LatencyRecorderTest {
    private LatencyRecorder latencyRecorder;

    @BeforeEach
    public void setup() {
        for (int i = 0; i < 500; i++) {
            latencyRecorder.addLatency((double) i);
        }
    }

    @Test
    public void checkMax() {
        assertThat(LatencyRecorder.getMax()).isEqualTo(499);
    }

    @Test
    public void checkMin() {
        assertThat(LatencyRecorder.getMin()).isEqualTo(0);
    }
}